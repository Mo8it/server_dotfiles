# Non interactive functions
function podman-default-create -w podman
    set -l container_name $argv[1]

    podman create \
        --name $container_name \
        --replace \
        --tz local \
        -e TZ=Europe/Berlin \
        $argv[2..]; or return 1

    podman generate systemd --no-header --new --name $container_name >~/.config/systemd/user/container-$container_name.service; or return 1

    systemctl --user enable --now container-$container_name
end

if status is-interactive
    # Disable greeting
    set -g fish_greeting

    # Interactive environment variables
    set -x EDITOR ~/.cargo/bin/hx

    # Abbreviations
    ## Replacements
    abbr -ag rm "trash"

    ## Safety options
    abbr -ag cp "cp -i"
    abbr -ag mv "mv -i"
    abbr -ag rmr "rm -r"

    ## Shortcuts
    abbr -ag pps "podman ps --format '{{.Names}}'"

    # Aliases
    alias cat "bat"
end
